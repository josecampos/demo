package ac.uk.shef;

public enum TriangleType {
	INVALID, SCALENE, EQUILATERAL, ISOSCELES
}
